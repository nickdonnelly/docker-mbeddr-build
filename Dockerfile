FROM debian:latest

RUN apt-get update
RUN apt-get install -y build-essential \
                         ant \
                         cbmc \
                         gdb \
                         graphviz \
                         openjdk-8-jdk \
                         openjdk-8-jre \
                         git

ENV JAVA_HOME /usr/lib/jvm/java-1.8.0-openjdk-amd64
ENV PATH $PATH:/usr/lib/jvm/java-1.8.0-openjdk-amd64/jre/bin:/usr/lib/jvm/java-1.8.0-openjdk-amd64/bin

RUN git clone https://github.com/Bartvhelvert/mbeddr.core.git 
WORKDIR mbeddr.core
RUN ls /usr/lib/jvm
RUN ./gradlew build_mbeddr
